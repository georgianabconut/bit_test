import helium
import random
from tests import selectors
from selenium.webdriver import ChromeOptions


def start_chrome_browser(url):
    options = ChromeOptions()
    options.add_argument('--start-maximized')
    helium.start_chrome(url, False, options)


def click_create_report_button():
    helium.click('CREATE REPORT')
    helium.wait_until(helium.Text("Details").exists, timeout_secs=10)


def select_type_or_company(dropdown, option):
    helium.wait_until(helium.S(dropdown).exists, timeout_secs=5)
    helium.click(helium.S(dropdown))
    helium.click(helium.S(option))


def fill_name_field():
    helium.write('Name ' + str(random.randint(1, 999999)), into='Enter name')


def click_save_button_and_assert():
    helium.click(helium.S(selectors.SAVE_BUTTON))
    helium.wait_until(helium.S('div[role="alertdialog"]').exists, timeout_secs=5)
