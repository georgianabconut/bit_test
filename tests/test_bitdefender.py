import helium
import pytest
from tests import selectors
from tests import functions


@pytest.mark.usefixtures()
def test_create_report_page():
    functions.start_chrome_browser('https://test-bitdef.web.app')

    functions.click_create_report_button()

    functions.select_type_or_company(selectors.TYPE_INPUT, selectors.EXECUTING_SUMMARY_OPTION)

    functions.select_type_or_company(selectors.COMPANY_INPUT, selectors.SAMPLE_COMPANY_OPTION)

    functions.fill_name_field()

    functions.click_save_button_and_assert()

    helium.kill_browser()
